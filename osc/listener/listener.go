package oscListener

import (
	"fmt"
	"github.com/hypebeast/go-osc/osc"
	"strconv"
	"strings"
)

type SoundboardStateType struct {
	Title     string  `json:"title"`
	Playing   bool    `json:"playing"`
	Done      bool    `json:"done"`
	Time      string  `json:"time"`
	Remaining string  `json:"remaining"`
	Progress  float32 `json:"progress"`
}

type TrackStateType struct {
	Name  string  `json:"name"`
	Sum   float32 `json:"sum"`
	Left  float32 `json:"left"`
	Right float32 `json:"right"`
}

type ReaperStateType struct {
	TimeString string `json:"timeString"`
	Play       bool   `json:"play"`
	Pause      bool   `json:"pause"`
	Stop       bool   `json:"stop"`
	Record     bool   `json:"record"`
	IfRepeat   bool   `json:"repeat"`
}

var SbStates [24]SoundboardStateType
var TrackStates [100]TrackStateType
var ReaperState = ReaperStateType{}

func Server(logAll bool) *osc.Server {
	ReaperState.TimeString = "?"
	addr := "127.0.0.1:9000"
	d := osc.NewStandardDispatcher()

	// catch dynamic addresses which contains track numbers or other dynamic parts
	d.AddMsgHandler("*", func(msg *osc.Message) {
		if logAll {
			//       if !strings.HasPrefix(msg.Address, "/track") && !strings.HasPrefix(msg.Address, "/master") && !strings.HasPrefix(msg.Address, "/beat") && !strings.HasPrefix(msg.Address, "/time") && !strings.HasPrefix(msg.Address, "/samples") && !strings.HasPrefix(msg.Address, "/frames") {

			fmt.Println(msg)
			//       }
		}

		switch msg.Address {
		case "/record":
			// time string will be shorted to one digit after comma.
			ReaperState.Record = msg.Arguments[0].(float32) == 1
		case "/play":
			// time string will be shorted to one digit after comma.
			ReaperState.Play = msg.Arguments[0].(float32) == 1
		case "/pause":
			// time string will be shorted to one digit after comma.
			ReaperState.Pause = msg.Arguments[0].(float32) == 1
		case "/stop":
			// time string will be shorted to one digit after comma.
			ReaperState.Stop = msg.Arguments[0].(float32) == 1
		case "/repeat":
			// time string will be shorted to one digit after comma.
			ReaperState.IfRepeat = msg.Arguments[0].(float32) == 1
		case "/time/str":
			// time string will be shorted to one digit after comma.
			timeStr := msg.Arguments[0].(string)
			displayLen := len(timeStr) - 2
			ReaperState.TimeString = string(timeStr[0:displayLen])
		case "/master/vu":
			TrackStates[0].Sum = msg.Arguments[0].(float32)
		case "/master/vu/L":
			TrackStates[0].Left = msg.Arguments[0].(float32)
		case "/master/vu/R":
			TrackStates[0].Right = msg.Arguments[0].(float32)
		}

		if strings.HasPrefix(msg.Address, "/ultraschall/soundboard/player/") {
			// match all soundboard actions
			addressParts := strings.Split(msg.Address, "/")

			playerNr, err := strconv.Atoi(addressParts[4])
			if err == nil {
				switch addressParts[5] {
				case "time":
					SbStates[playerNr-1].Time = msg.Arguments[0].(string)
					fmt.Println(SbStates)
				case "progress":
					SbStates[playerNr-1].Progress = msg.Arguments[0].(float32)
				case "remaining":
					SbStates[playerNr-1].Remaining = msg.Arguments[0].(string)
				case "play":
					SbStates[playerNr-1].Playing = msg.Arguments[0].(float32) == 1
				case "title":
					SbStates[playerNr-1].Title = msg.Arguments[0].(string)
				default:
					fmt.Println(strconv.Itoa(playerNr) + ": " + addressParts[5])
					fmt.Println(msg)
				}
			}
		} else if strings.HasPrefix(msg.Address, "/track/") {
			if strings.Index(msg.Address, "/vu") > 6 {
				// match all track vus
				addressParts := strings.Split(msg.Address, "/")
				trackNr, err := strconv.Atoi(addressParts[2])
				if err == nil {
					if len(addressParts) == 4 {
						TrackStates[trackNr].Sum = msg.Arguments[0].(float32)
					} else if addressParts[4] == "L" {
						TrackStates[trackNr].Left = msg.Arguments[0].(float32)
					} else if addressParts[4] == "R" {
						TrackStates[trackNr].Right = msg.Arguments[0].(float32)
					}
				}
			}
			//       if strings.Index(msg.Address, "/name") != 6 {
			//         addressParts := strings.Split(msg.Address, "/");
			//         trackNr, err2 := strconv.Atoi(addressParts[2]);
			//         if err2 == nil {
			//           TrackStates[trackNr].Name = msg.Arguments[0].(string)
			//         }
			//       }
			//     } else {
			//       fmt.Println(msg)
		}
	})

	fmt.Println("Start listener server...")

	server := &osc.Server{
		Addr:       addr,
		Dispatcher: d,
	}

	return server
}
