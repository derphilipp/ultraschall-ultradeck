package oscClient

import (
	"github.com/hypebeast/go-osc/osc"
	"strconv"
)

var Client *osc.Client
var ClientSoundboard *osc.Client

func init() {
	Client = osc.NewClient("localhost", 8000)
	ClientSoundboard = osc.NewClient("localhost", 8050)
}

func ExecActionByName(action string) {
	msg := osc.NewMessage("/action/str")
	msg.Append(action)
	Client.Send(msg)
}

func ExecActionById(actionId int) {
	msg := osc.NewMessage("/action")
	msg.Append(actionId)
	Client.Send(msg)
}

func SoundboardPlay(player int) {
	msg := osc.NewMessage("/ultraschall/soundboard/player/" + strconv.Itoa(player) + "/play")
	msg.Append(float32(1))
	ClientSoundboard.Send(msg)
}

func SoundboardPause(player int) {
	msg := osc.NewMessage("/ultraschall/soundboard/player/" + strconv.Itoa(player) + "/pause")
	msg.Append(float32(1))
	ClientSoundboard.Send(msg)
}

func SoundboardStop(player int) {
	msg := osc.NewMessage("/ultraschall/soundboard/player/" + strconv.Itoa(player) + "/stop")
	msg.Append(float32(1))
	ClientSoundboard.Send(msg)
}

func TrackToggleMute(track int) {
	msg := osc.NewMessage("/action/" + strconv.Itoa(14+track*8))
	msg.Append(float32(1))
	Client.Send(msg)
}

func TrackMute(track int) {
	msg := osc.NewMessage("/track/" + strconv.Itoa(track) + "/mute")
	msg.Append(float32(1))
	Client.Send(msg)
}

func TrackUnmute(track int) {
	msg := osc.NewMessage("/track/" + strconv.Itoa(track) + "/mute")
	msg.Append(float32(0))
	Client.Send(msg)
}

func Play() {
	msg := osc.NewMessage("/play")
	msg.Append(float32(1))
	Client.Send(msg)
}

func Pause() {
	msg := osc.NewMessage("/pause")
	msg.Append(float32(1))
	Client.Send(msg)
}

func Stop() {
	msg := osc.NewMessage("/stop")
	msg.Append(float32(1))
	Client.Send(msg)
}

func Record() {
	msg := osc.NewMessage("/record")
	msg.Append(float32(1))
	Client.Send(msg)
}

func Repeat() {
	msg := osc.NewMessage("/repeat")
	msg.Append(float32(1))
	Client.Send(msg)
}

func TransportStart() {
	ExecActionByName("_Ultraschall_Go_To_Start_Of_Project")
}

func TransportEnd() {
	ExecActionByName("_Ultraschall_Go_To_End_Of_Project")
}

func TransportPreviousMarkerOrStart() {
	ExecActionByName("_Ultraschall_Go_To_Previous_Marker_Projectstart")
}

func TransportNextMarkerOrEnd() {
	ExecActionByName("_Ultraschall_Go_To_Next_Marker_Projectend")
}

func StartRecordingAtEnd() {
	ExecActionByName("_Ultraschall_Start_Recording_at_End")
}

func GoToCursor() {
	ExecActionByName("_Ultraschall_Go_To_Cursor")
}

func FocusPosition() {
	ExecActionByName("_Ultraschall_FocusPosition")
}

func FocusZoomIn() {
	ExecActionByName("_Ultraschall_FocusZoomIn")
}

func ImportChapterMarkersFromWav() {
	ExecActionByName("_Ultraschall_ImportChapterMarkersFromWav")
}

func InsertAndArmNewAudioTrack() {
	ExecActionByName("_Ultraschall_Insert_And_Arm_New_Audio_Track")
}

func PlayFromEditCursorPosition() {
	ExecActionByName("_Ultraschall_Play_From_Editcursor_Position")
}

func PlayFromInpoint() {
	ExecActionByName("_Ultraschall_PlayFromInpoint")
}

func PlayFromOutpoint() {
	ExecActionByName("_Ultraschall_PlayFromOutpoint")
}

func SelectItemAndMoveEditCursor() {
	ExecActionByName("_Ultraschall_Select_Item_And_Move_Edit_Cursor")
}

func SelectItemAndMoveEditCursorWithFollowModeWorkaround() {
	ExecActionByName("_Ultraschall_Select_Item_Move_Editcursor_with_workaround")
}

func SkipLoopPlayback() {
	ExecActionByName("_Ultraschall_SkipLoopPlayback")
}

func SkipLoopPlaybackAbsolute() {
	ExecActionByName("_Ultraschall_SkipLoopPlayback_Absolute")
}

func SliceAndAddMetadata() {
	ExecActionByName("_Ultraschall_Slice_and_AddMetadata")
}

func SplitAndDelete() {
	ExecActionByName("_Ultraschall_Split_and_Delete")
}

func UnselectAll() {
	ExecActionByName("_Ultraschall_Unselect_All")
}

func ZoomOutHorizontal() {
	ExecActionByName("_Ultraschall_Zoom_Out_Horizontal")
}

func ZoomInHorizontal() {
	ExecActionByName("_Ultraschall_Zoom_In_Horizontal")
}

func PauseRecording() {
	ExecActionByName("_Ultraschall_Pause_Recording")
}

func ZoomInCenterToCursorPosition() {
	ExecActionByName("_Ultraschall_ZoomIn_Center_To_Cursorposition")
}

func ZoomOutCenterToCursorPosition() {
	ExecActionByName("_Ultraschall_ZoomOut_Center_To_Cursorposition")
}

func ZoomOutCenteredCursorPosition() {
	ExecActionByName("_Ultraschall_Zoom_Out_Centered_Cursorposition")
}

func RecordingStartAtEnd() {
	ExecActionByName("_Ultraschall_Start_Recording_at_End")
}

// func  () {
//   ExecActionByName("")
// }
//
// func  () {
//   ExecActionByName("")
// }

func SetMatrixPreshow() {
	ExecActionByName("_Ultraschall_set_Matrix_Preshow")
}

func SetMatrixRecording() {
	ExecActionByName("_Ultraschall_set_Matrix_Recording")
}

func SetMatrixEditing() {
	ExecActionByName("_Ultraschall_set_Matrix_Editing")
}

func SetChapterMarker() {
	ExecActionByName("_Ultraschall_Set_Marker")
}

func SetChapterMarkerBackInTime() {
	ExecActionByName("_Ultraschall_Insert_Chapter_Marker_Back_In_Time")
}

func SetEditMarker() {
	ExecActionByName("_Ultraschall_Set_Edit")
}

func SetEditMarkerAtPlayPos() {
	ExecActionByName("_Ultraschall_Set_Edit_Play")
}

func ToggleFollowMode() {
	ExecActionByName("_Ultraschall_Toggle_Follow")
}

func ToggleMagicRouting() {
	ExecActionByName("_Ultraschall_Toggle_Magicrouting")
}

func ToggleOnAir() {
	ExecActionByName("_Ultraschall_OnAir")
}

func SetViewSetup() {
	ExecActionByName("_Ultraschall_Set_View_Setup")
}

func SetViewRecord() {
	ExecActionByName("_Ultraschall_Set_View_Record")
}

func SetViewEdit() {
	ExecActionByName("_Ultraschall_Set_View_Edit")
}

func SetViewStoryboard() {
	ExecActionByName("_Ultraschall_Set_View_Story")
}

func DeleteLastMarker() {
	ExecActionByName("_Ultraschall_Delete_Last_Marker")
}
