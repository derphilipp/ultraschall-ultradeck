package oscdata

type SoundboardState struct {
	title     string  `json:"title"`
	playing   bool    `json:"playing"`
	time      string  `json:"time"`
	remaining string  `json:"remaining"`
	progress  float32 `json:"progress"`
}

type VUMeter struct {
	sum   float32 `json:"sum"`
	left  float32 `json:"left"`
	right float32 `json:"right"`
}

var SbStates [24]SoundboardState
var MasterVu VUMeter
var TracksVu [20]VUMeter
