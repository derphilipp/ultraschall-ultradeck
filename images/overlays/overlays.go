package imagesOverlays

import (
	"image"
	_ "image/png"
	"os"
)

// Overlays have transparent backgrounds
var Overlays map[string]image.Image

func init() {
	Overlays = make(map[string]image.Image)
	Overlays["ad"] = LoadImg("./action/images/overlays/ad.png")
	Overlays["applause"] = LoadImg("./action/images/overlays/applause.png")
	Overlays["bigbluebutton"] = LoadImg("./action/images/overlays/bigbluebutton.png")
	Overlays["hot"] = LoadImg("./action/images/overlays/hot.png")
	Overlays["intro"] = LoadImg("./action/images/overlays/intro.png")
	Overlays["jitsi"] = LoadImg("./action/images/overlays/jitsi.png")
	Overlays["microphone"] = LoadImg("./action/images/overlays/microphone.png")
	Overlays["mobile"] = LoadImg("./action/images/overlays/mobile.png")
	Overlays["money"] = LoadImg("./action/images/overlays/money.png")
	Overlays["outro"] = LoadImg("./action/images/overlays/outro.png")
	Overlays["skype"] = LoadImg("./action/images/overlays/skype.png")
	Overlays["smiley_angry"] = LoadImg("./action/images/overlays/smiley_angry.png")
	Overlays["smiley_beam"] = LoadImg("./action/images/overlays/smiley_beam.png")
	Overlays["smiley_grin"] = LoadImg("./action/images/overlays/smiley_grin.png")
	Overlays["smiley_squint"] = LoadImg("./action/images/overlays/smiley_squint.png")
	Overlays["smiley_squint_tears"] = LoadImg("./action/images/overlays/smiley_squint_tears.png")
	Overlays["soundboard"] = LoadImg("./action/images/overlays/soundboard.png")
	Overlays["soundboard_simple"] = LoadImg("./action/images/overlays/soundboard_simple.png")
	Overlays["studiolink"] = LoadImg("./action/images/overlays/studiolink.png")
	Overlays["thumb_down"] = LoadImg("./action/images/overlays/thumb_down.png")
	Overlays["thumb_up"] = LoadImg("./action/images/overlays/thumb_up.png")
	Overlays["zoom"] = LoadImg("./action/images/overlays/zoom.png")
	Overlays["letter_a"] = LoadImg("./action/images/overlays/letters/a.png")
	Overlays["letter_b"] = LoadImg("./action/images/overlays/letters/b.png")
	Overlays["letter_c"] = LoadImg("./action/images/overlays/letters/c.png")
	Overlays["letter_d"] = LoadImg("./action/images/overlays/letters/d.png")
	Overlays["letter_e"] = LoadImg("./action/images/overlays/letters/e.png")
	Overlays["letter_f"] = LoadImg("./action/images/overlays/letters/f.png")
	Overlays["letter_g"] = LoadImg("./action/images/overlays/letters/g.png")
	Overlays["letter_h"] = LoadImg("./action/images/overlays/letters/h.png")
	Overlays["letter_i"] = LoadImg("./action/images/overlays/letters/i.png")
	Overlays["letter_j"] = LoadImg("./action/images/overlays/letters/j.png")
	Overlays["letter_k"] = LoadImg("./action/images/overlays/letters/k.png")
	Overlays["letter_l"] = LoadImg("./action/images/overlays/letters/l.png")
	Overlays["letter_m"] = LoadImg("./action/images/overlays/letters/m.png")
	Overlays["letter_n"] = LoadImg("./action/images/overlays/letters/n.png")
	Overlays["letter_o"] = LoadImg("./action/images/overlays/letters/o.png")
	Overlays["letter_p"] = LoadImg("./action/images/overlays/letters/p.png")
	Overlays["letter_q"] = LoadImg("./action/images/overlays/letters/q.png")
	Overlays["letter_r"] = LoadImg("./action/images/overlays/letters/r.png")
	Overlays["letter_s"] = LoadImg("./action/images/overlays/letters/s.png")
	Overlays["letter_t"] = LoadImg("./action/images/overlays/letters/t.png")
	Overlays["letter_u"] = LoadImg("./action/images/overlays/letters/u.png")
	Overlays["letter_v"] = LoadImg("./action/images/overlays/letters/v.png")
	Overlays["letter_w"] = LoadImg("./action/images/overlays/letters/w.png")
	Overlays["letter_x"] = LoadImg("./action/images/overlays/letters/x.png")
	Overlays["letter_y"] = LoadImg("./action/images/overlays/letters/y.png")
	Overlays["letter_z"] = LoadImg("./action/images/overlays/letters/z.png")

}

func LoadImg(filename string) image.Image {
	infile, err := os.Open(filename)
	if err != nil {
		// replace this with real error handling
		return nil
	}
	defer infile.Close()

	img, _, err := image.Decode(infile)
	if err != nil {
		// replace this with real error handling
		return nil
	}

	return img
}
