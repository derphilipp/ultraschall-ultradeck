package imagesCombine

import (
	"image"
	"image/draw"
)

func CombineTwo(bgImg image.Image, overlayImg image.Image) image.Image {
	bounds := bgImg.Bounds()
	newImg := image.NewRGBA(bounds)
	draw.Draw(newImg, bounds, bgImg, image.ZP, draw.Src)
	draw.Draw(newImg, bounds, overlayImg, image.ZP, draw.Over)
	return newImg
}
