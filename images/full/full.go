package imagesFull

import (
	"image"
	_ "image/png"
	"os"
)

// Transport Icons
var TransportBegin image.Image
var TransportEnd image.Image
var TransportPause image.Image
var TransportPauseActive image.Image
var TransportPlay image.Image
var TransportPlayActive image.Image
var TransportRecord image.Image
var TransportRecordActive image.Image
var TransportRepeat image.Image
var TransportRepeatActive image.Image
var TransportStop image.Image

var MagicRouting image.Image
var MagicRoutingActive image.Image
var MatrixPreshow image.Image
var MatrixPreshowActive image.Image
var MatrixRecording image.Image
var MatrixRecordingActive image.Image
var MatrixEditing image.Image
var MatrixEditingActive image.Image

var OnAir image.Image
var OnAirActive image.Image

var ViewSetup image.Image
var ViewSetupActive image.Image
var ViewRecord image.Image
var ViewRecordActive image.Image
var ViewEdit image.Image
var ViewEditActive image.Image
var ViewStoryboard image.Image
var ViewStoryboardActive image.Image

var MarkerChapter image.Image
var MarkerChapterBackInTime image.Image
var MarkerEdit image.Image

func init() {
	TransportBegin = loadImg("./action/images/transport/begin.png")
	TransportEnd = loadImg("./action/images/transport/end.png")
	TransportPause = loadImg("./action/images/transport/pause.png")
	TransportPauseActive = loadImg("./action/images/transport/pause_active.png")
	TransportPlay = loadImg("./action/images/transport/play.png")
	TransportPlayActive = loadImg("./action/images/transport/play_active.png")
	TransportRepeat = loadImg("./action/images/transport/repeat.png")
	TransportRepeatActive = loadImg("./action/images/transport/repeat_active.png")
	TransportRecord = loadImg("./action/images/transport/record.png")
	TransportRecordActive = loadImg("./action/images/transport/record_active.png")
	TransportStop = loadImg("./action/images/transport/stop.png")

	MagicRouting = loadImg("./action/images/toggles/magicrouting.png")
	MagicRoutingActive = loadImg("./action/images/toggles/magicrouting_active.png")
	MatrixPreshow = loadImg("./action/images/toggles/preshow.png")
	MatrixPreshowActive = loadImg("./action/images/toggles/preshow_active.png")
	MatrixRecording = loadImg("./action/images/toggles/recording.png")
	MatrixRecordingActive = loadImg("./action/images/toggles/recording_active.png")
	MatrixEditing = loadImg("./action/images/toggles/editing.png")
	MatrixEditingActive = loadImg("./action/images/toggles/editing_active.png")

	OnAir = loadImg("./action/images/toggles/onair.png")
	OnAirActive = loadImg("./action/images/toggles/onair_active.png")

	ViewSetup = loadImg("./action/images/views/setup.png")
	ViewSetupActive = loadImg("./action/images/views/setup_active.png")
	ViewRecord = loadImg("./action/images/views/record.png")
	ViewRecordActive = loadImg("./action/images/views/record_active.png")
	ViewEdit = MatrixEditing
	ViewEditActive = MatrixEditingActive
	ViewStoryboard = loadImg("./action/images/views/storyboard.png")
	ViewStoryboardActive = loadImg("./action/images/views/storyboard_active.png")

	MarkerChapter = loadImg("./action/images/marker/chapter.png")
	MarkerChapterBackInTime = loadImg("./action/images/marker/chapter_backintime.png")
	MarkerEdit = loadImg("./action/images/marker/edit.png")

}

func loadImg(filename string) image.Image {
	infile, err := os.Open(filename)
	if err != nil {
		// replace this with real error handling
		panic(err.Error())
	}
	defer infile.Close()

	img, _, err := image.Decode(infile)
	if err != nil {
		// replace this with real error handling
		panic(err.Error())
	}

	return img
}
