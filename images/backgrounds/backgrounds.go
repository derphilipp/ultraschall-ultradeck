package imagesBackgrounds

import (
	"image"
	"image/color"
	_ "image/png"
)

// "Empty" Black Background
var Black image.Image
var Transparent image.Image

func init() {
	Black = Filled(color.RGBA{0, 0, 0, 0xff})
	Transparent = Filled(color.RGBA{0, 0, 0, 0x00})
}

func Filled(col color.RGBA) image.Image {
	img := image.NewRGBA(image.Rect(0, 0, 72, 72))
	for x := 0; x < 72; x++ {
		for y := 0; y < 72; y++ {
			img.SetRGBA(x, y, col)
		}
	}
	return img
}

func ProgressVerticalBottomUpFullWidth(filledY int, fillColor color.RGBA, bgColor color.RGBA) image.Image {
	img := image.NewRGBA(image.Rect(0, 0, 72, 72))
	for x := 0; x < 72; x++ {
		for y := 0; y < 72; y++ {
			if y > 72-filledY {
				img.SetRGBA(x, y, fillColor)
			} else {
				img.SetRGBA(x, y, bgColor)
			}
		}
	}
	return img
}

func ProgressHorizontalLeftRightFullHeight(filledX int, fillColor color.RGBA, bgColor color.RGBA) image.Image {
	img := image.NewRGBA(image.Rect(0, 0, 72, 72))
	// 	filledX := int(72 * progress);
	for x := 0; x < 72; x++ {
		for y := 0; y < 72; y++ {
			if x < filledX {
				img.SetRGBA(x, y, fillColor)
			} else {
				img.SetRGBA(x, y, bgColor)
			}
		}
	}

	return img
}

func ProgressHorizontalLeftRightHeight(progress float64, fillColor color.RGBA, bgColor color.RGBA, height int) image.Image {
	img := image.NewRGBA(image.Rect(0, 0, 72, 72))
	filledX := int(72 * progress)
	for x := 0; x < 72; x++ {
		for y := 0; y < 72; y++ {
			if y >= 72-height && x < filledX {
				img.SetRGBA(x, y, fillColor)
			} else {
				img.SetRGBA(x, y, bgColor)
			}
		}
	}
	return img
}
