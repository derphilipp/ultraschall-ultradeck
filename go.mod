module gitlab.com/derphilipp/ultraschall-ultradeck

go 1.15

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hypebeast/go-osc v0.0.0-20200115085105-85fee7fed692
	github.com/samwho/streamdeck v0.0.0-20190725183037-2b866fdcb4a6
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/teacat/noire v1.0.0
)
