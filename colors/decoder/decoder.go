package colorsDecoder

import (
	"image/color"
	"strconv"

	"github.com/teacat/noire"
)

func RGBHexToRGBA(rgbHex string, alpha uint8, defaultColor color.RGBA) (color.RGBA, error) {
	if len(rgbHex) != 7 {
		return defaultColor, nil
	}
	red, errRed := strconv.ParseUint(string(rgbHex[1:3]), 16, 8)
	if errRed != nil {
		return defaultColor, errRed
	}
	green, errGreen := strconv.ParseUint(string(rgbHex[3:5]), 16, 8)
	if errGreen != nil {
		return defaultColor, errGreen
	}
	blue, errBlue := strconv.ParseUint(string(rgbHex[5:7]), 16, 8)
	if errBlue != nil {
		return defaultColor, errBlue
	}
	return color.RGBA{uint8(red), uint8(green), uint8(blue), alpha}, nil
}

func LightenHex(hex string, percent float64) color.RGBA {
	col := noire.NewHex(hex).Lighten(percent)
	return color.RGBA{uint8(col.Red), uint8(col.Green), uint8(col.Blue), uint8(col.Alpha * 255)}
}

func DarkenHex(hex string, percent float64) color.RGBA {
	col := noire.NewHex(hex).Darken(percent)
	return color.RGBA{uint8(col.Red), uint8(col.Green), uint8(col.Blue), uint8(col.Alpha * 255)}
}

func LightenCol(col color.RGBA, percent float64) color.RGBA {
	noireCol := noire.NewRGBA(float64(col.R), float64(col.G), float64(col.B), float64(col.A)/255).Lighten(percent)
	return color.RGBA{uint8(noireCol.Red), uint8(noireCol.Green), uint8(noireCol.Blue), uint8(noireCol.Alpha * 255)}
}

func DarkenCol(col color.RGBA, percent float64) color.RGBA {
	noireCol := noire.NewRGBA(float64(col.R), float64(col.G), float64(col.B), float64(col.A)/255).Darken(percent)
	return color.RGBA{uint8(noireCol.Red), uint8(noireCol.Green), uint8(noireCol.Blue), uint8(noireCol.Alpha * 255)}
}
