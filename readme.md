# Ultradeck - A Stream Deck Plugin for Ultraschall

Ultradeck is a plugin for Elgato Stream Deck written in [GO](https://golang.org/).
You can configure the Stream Deck to match your needs to control Ultraschall via OSC.
The advantage of controlling a DAW (Digital Audio Workstation) via OSC is, that the DAW has not be the focused application to control it.

This plugin is proudly presented by the Ultraschall Team and written by [Christian Daxberger (Daxi)](https://sendegate.de/u/daxi).
We hope that you will enjoy this plugin.

If you need any additional feature or run into issues, please let us know. 
You can contact us at the German Podcasting Community [Sendegate](https://sendegate.de/).

## Get Ultraschall

Ultrashall is a ditigal audio workstation based on Reaper.
The full download and installation guide can be found on [ultraschall.fm](http://ultraschall.fm)

## Features

### Transport
You can control Ultraschall with Ultradeck. For that you can select one of the following actions:
* Stop: Sends the stop command to Ultraschall
* Play: Playback or Resume recording if in record mode
* Pause: Pauses playback or recording**
* Record: Starts recording

Play and Record exists twice - the difference is the " + Time" suffix.
If you select one of these actions, the playback or record position will be displayed in your Stream Deck.

**Known issue:**
Reaper sends strange command if you pause recording. At OSC level it looks identical with a playback pause.
This is the reason why in Ultraschall the buttons Play, Pause and Record are highlighted and on your Stream Deck only the Pause button is highlighted.

### Track

### Routing Matrix

***Knwon issue:***
Reaper does not send information about the active routing matrix via OSC.
The plugin currently can not identify which mode is active and all matrix buttons does not change the active state. 

### Soundboard

### On Air


## Configure Ultraschall

Currently the ultradeck plugin has no global settings available to configure the ports for the OSC server and client part.

* OSC server port: 9000
* OSC client port: 8050

**Setup Ultraschall Soundboard:**

1. Add a Soundboard Track to Ultraschall (Menu "Podcast" / "Insert StudioLink and Soundboard tracks" / "Insert Ultraschall soundboard track")
2. A new track will be added with an effect ("FX") configured.
3. Click on the left bottom side with the Track Volume controls and click the "FX" button on the soundboard track
4. The Effects configuration will be displayed in the right bottom area of Ultraschall
5. Select "Soundboard" (and keep the checkbox ticked, please klick on the title)
6. Click on the cog at the soundboard interface to open the configuration dialog
    * Enable "Receive" if not enabled
        * Hostname: The hostname should be automatically filled
        * Port: 8050
    * Enable "Send" if not enabled
        * Hostname: 127.0.0.1
        * Port: 9000
    * Enable "Repeater" if not enabled
        * Hostname: 127.0.0.1
        * Port: 8000 (can be changed but has to match the Reaper settings below)

**Setup Reaper:**

1. Open Reaper Preferences
2. Go to: Control/OSC/Web
3. Add a control surface mode: "OSC (Open Sound Control)"
    * Mode: "Configure device IP+local port"
    * Device port: 9000
    * Device IP: 0.0.0.0
    * Local listen port: same as Soundboard Repeater (e.g. 8000)
    * Local IP should be filled automatically

**Done**

Ultradeck uses only default OSC commands which are provided by Reaper or the soundboard.
You can start now to configure the Stream Deck Plugin.

## Build Binary

### Mac
```shell script
cd fm.ultraschall.ultradeck.sdPlugin/src
go build -o ../ultradeck ultradeck.go
```

### Windows 10
This plugin is currently not tested and compiled for Windows 10.
Feel free to contact us if you want to help us with testing and reporting bugs.

## Known issues

### General
* Sometimes the configuration from property inspector will not be accepted. Please click on an other action and back to the newly configured to double check if your planned configuration works.

### Track Action
* Action Mute Track can mute the track but Unmute will not be accepted by Ultraschall in record mode. Please use "Toggle mute" for both actions

### Soundboard
* User defined overlays will not be displayed immediatly. Please start drag and drop on the action button (can be dropped on the same slot) or switch the profile or folder to refresh the user defined overlay

## Credits

Icons for Overlays used from Fontawesome 5 Free editon.