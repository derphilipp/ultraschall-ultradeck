package main

import (
	"context"
	"os"

	"github.com/samwho/streamdeck"

	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/counter"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/customaction"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/debug"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/marker"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/routing"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/soundboard"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/time"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/track"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/transport"
	"gitlab.com/derphilipp/ultraschall-ultradeck/actions/view"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/listener"
	"gitlab.com/derphilipp/ultraschall-ultradeck/webapi"
)

func main() {
	go oscListener.Server(false).ListenAndServe()
	go webApi.PollWebAPI()
	ctx := context.Background()
	run(ctx)
}

func run(ctx context.Context) error {
	params, err := streamdeck.ParseRegistrationParams(os.Args)
	if err != nil {
		return err
	}

	client := streamdeck.NewClient(ctx, params)

	actionCounter.SetupAction(client)
	actionTrack.SetupAction(client)
	actionSoundboard.SetupAction(client)
	actionTime.SetupAction(client)
	actionTransport.SetupAction(client)
	actionRouting.SetupAction(client)
	actionCustomAction.SetupAction(client)
	actionMarker.SetupAction(client)
	actionView.SetupAction(client)

	actionDebug.SetupAction(client)

	return client.Run()
}
