package webApi

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type UltraschallStateType struct {
	ViewSetup              bool
	ViewRecord             bool
	ViewEdit               bool
	ViewStory              bool
	MagicRouting           bool
	RoutingMatrixPreshow   bool
	RoutingMatrixRecording bool
	RoutingMatrixEditing   bool
	OnAir                  bool
	FollowMode             bool
	RecordPause            bool
}

var State UltraschallStateType

func init() {
	State = UltraschallStateType{}
}

func PollWebAPI() {
	for range time.Tick(time.Second / 5) {
		fmt.Println("...poll")
		resp, err := http.Get("http://localhost:8081/_/GET/_Ultraschall_Set_View_Setup;GET/_Ultraschall_Set_View_Record;GET/_Ultraschall_Set_View_Edit;GET/_Ultraschall_Set_View_Story;GET/_Ultraschall_Toggle_Follow;GET/_Ultraschall_OnAir;GET/_Ultraschall_Toggle_Magicrouting;GET/_Ultraschall_set_Matrix_Preshow;GET/_Ultraschall_set_Matrix_Recording;GET/_Ultraschall_set_Matrix_Editing;GET/_Ultraschall_Pause_Recording")

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			lines := strings.Split(string(body), "\n")
			for _, l := range lines {
				cols := strings.Split(l, "\t")

				if cols[0] == "CMDSTATE" {
					cmdState := false
					if len(cols) == 3 && cols[2] == "1" {
						cmdState = true
					}
					switch cols[1] {
					case "_Ultraschall_Set_View_Setup":
						State.ViewSetup = cmdState
					case "_Ultraschall_Set_View_Record":
						State.ViewRecord = cmdState
					case "_Ultraschall_Set_View_Edit":
						State.ViewEdit = cmdState
					case "_Ultraschall_Set_View_Story":
						State.ViewStory = cmdState
					case "_Ultraschall_Toggle_Follow":
						State.FollowMode = cmdState
					case "_Ultraschall_OnAir":
						State.OnAir = cmdState
					case "_Ultraschall_Toggle_Magicrouting":
						State.MagicRouting = cmdState
					case "_Ultraschall_set_Matrix_Preshow":
						State.RoutingMatrixPreshow = cmdState
					case "_Ultraschall_set_Matrix_Recording":
						State.RoutingMatrixRecording = cmdState
					case "_Ultraschall_set_Matrix_Editing":
						State.RoutingMatrixEditing = cmdState
					case "_Ultraschall_Pause_Recording":
						State.RecordPause = cmdState
					}
				}
			}
			fmt.Println(State)
		}
	}
}
