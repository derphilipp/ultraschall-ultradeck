package actionView

import (
	"context"
	"encoding/json"
	"image"
	"time"
	// 	"os"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"gitlab.com/derphilipp/ultraschall-ultradeck/images/backgrounds"
	"gitlab.com/derphilipp/ultraschall-ultradeck/images/full"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/client"
	"gitlab.com/derphilipp/ultraschall-ultradeck/webapi"
)

type ViewSettings struct {
	Action string `json:"action"`
}

var Settings map[string]*ViewSettings
var CurrentImage map[string]string

func init() {
	Settings = make(map[string]*ViewSettings)
	CurrentImage = make(map[string]string)
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.view")

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &ViewSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &ViewSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}
		client.SetTitle(ctx, s.Action, streamdeck.HardwareAndSoftware)

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &ViewSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			Settings[event.Context] = s
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		CurrentImage[event.Context] = ""
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if ok {
			switch s.Action {
			case "setup":
				oscClient.SetViewSetup()
			case "record":
				oscClient.SetViewRecord()
			case "edit":
				oscClient.SetViewEdit()
			case "storyboard":
				oscClient.SetViewStoryboard()
			}
		}

		return nil
	})

	go func() {
		for range time.Tick(time.Second / 10) {
			for ctxStr := range Settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				//           s, _ := Settings[ctxStr]

				img := getTypeImage(ctxStr)
				if img != nil {
					bg, err1 := streamdeck.Image(img)
					if err1 == nil {
						client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
					}
				}
			}
		}
	}()
}

func getTypeImage(ctxStr string) image.Image {
	s := Settings[ctxStr]
	cI := CurrentImage[ctxStr]

	switch s.Action {
	case "setup":
		if webApi.State.ViewSetup {
			if cI != "setup_active" {
				CurrentImage[ctxStr] = "setup_active"
				return imagesFull.ViewSetupActive
			}
		} else {
			if cI != "setup" {
				CurrentImage[ctxStr] = "setup"
				return imagesFull.ViewSetup
			}
		}
	case "record":
		if webApi.State.ViewRecord {
			if cI != "record_active" {
				CurrentImage[ctxStr] = "record_active"
				return imagesFull.ViewRecordActive
			}
		} else {
			if cI != "record" {
				CurrentImage[ctxStr] = "record"
				return imagesFull.ViewRecord
			}
		}
	case "edit":
		if webApi.State.ViewEdit {
			if cI != "edit_active" {
				CurrentImage[ctxStr] = "edit_active"
				return imagesFull.ViewEditActive
			}
		} else {
			if cI != "edit" {
				CurrentImage[ctxStr] = "edit"
				return imagesFull.ViewEdit
			}
		}
	case "storyboard":
		if webApi.State.ViewStory {
			if cI != "storyboard_active" {
				CurrentImage[ctxStr] = "storyboard_active"
				return imagesFull.ViewStoryboardActive
			}
		} else {
			if cI != "storyboard" {
				CurrentImage[ctxStr] = "storyboard"
				return imagesFull.ViewStoryboard
			}
		}
	default:
		if cI != "black" {
			CurrentImage[ctxStr] = "black"
			return imagesBackgrounds.Black
		}
	}

	return nil
}
