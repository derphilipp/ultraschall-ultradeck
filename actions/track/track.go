package actionTrack

import (
	"context"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"time"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/client"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/listener"

	"gitlab.com/derphilipp/ultraschall-ultradeck/colors/decoder"
	"gitlab.com/derphilipp/ultraschall-ultradeck/images/backgrounds"
	"gitlab.com/derphilipp/ultraschall-ultradeck/images/combine"
	"gitlab.com/derphilipp/ultraschall-ultradeck/images/overlays"
)

type TrackSettings struct {
	Track         int    `json:"track,string" min:"1" max:"99"`
	ShowVU        int    `json:"showvu,string" min:"0" max:"1"`
	Overlay       string `json:"overlay"`
	OnKeyPress    string `json:"onkeypress"`
	OnKeyRelease  string `json:"onkeyrelease"`
	ColorVUNormal string `json:"vucolor"`
	ColorVUMiddle string
	ColorVUHigh   string
}

type TrackCache struct {
	FilledPx int
	Images   [73]string // 0-72 filled px
}

var Settings map[string]*TrackSettings
var Cache map[string]*TrackCache

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.track")
	Settings = make(map[string]*TrackSettings)
	Cache = make(map[string]*TrackCache)

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &TrackSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &TrackSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}
		Cache[event.Context] = &TrackCache{}

		buildImageCache(event.Context)
		client.SetImage(ctx, Cache[event.Context].Images[0], streamdeck.HardwareAndSoftware)

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &TrackSettings{}
			Settings[event.Context] = s
		}
		Cache[event.Context] = &TrackCache{}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		buildImageCache(event.Context)
		client.SetImage(ctx, Cache[event.Context].Images[0], streamdeck.HardwareAndSoftware)

		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if !ok {
			return fmt.Errorf("couldn't find settings for context %v", event.Context)
		}

		switch s.OnKeyPress {
		case "mute":
			oscClient.TrackMute(s.Track)
		case "unmute":
			oscClient.TrackUnmute(s.Track)
		case "toggle":
			oscClient.TrackToggleMute(s.Track)
		}

		return nil
	})

	action.RegisterHandler(streamdeck.KeyUp, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if !ok {
			return fmt.Errorf("couldn't find settings for context %v", event.Context)
		}

		switch s.OnKeyRelease {
		case "mute":
			oscClient.TrackMute(s.Track)
		case "unmute":
			oscClient.TrackUnmute(s.Track)
		case "toggle":
			oscClient.TrackToggleMute(s.Track)
		}

		return nil
	})

	go func() {
		for range time.Tick(time.Second / 10) {
			for ctxStr := range Settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				s := Settings[ctxStr]
				if s.ShowVU == 1 {
					// only update image if vu should be displayed
					filledPx := int(72 * float64(oscListener.TrackStates[s.Track].Sum))
					c := Cache[ctxStr]

					if c.FilledPx != filledPx {
						client.SetImage(ctx, c.Images[filledPx], streamdeck.HardwareAndSoftware)
						c.FilledPx = filledPx
					}
				}
			}
		}
	}()
}

func buildImageCache(ctxStr string) {
	var imgOverlay image.Image
	var img image.Image

	s := Settings[ctxStr]
	vuColor, _ := colorsDecoder.RGBHexToRGBA(s.ColorVUNormal, 255, color.RGBA{0, 255, 0, 255})

	if s.Overlay == "_userdefined" {
		// TODO: User defined overlay or not found in overlays
		imgOverlay = nil
	} else {
		imgOverlay, _ = imagesOverlays.Overlays[s.Overlay]
	}

	for i := 0; i <= 72; i++ {
		// Prerender all fill states as data url to sent to streamdeck without rendering while update
		if s.ShowVU == 1 {
			imgBg := imagesBackgrounds.ProgressVerticalBottomUpFullWidth(i, vuColor, color.RGBA{0, 0, 0, 0xff})
			if imgOverlay != nil {
				img = imagesCombine.CombineTwo(imgBg, imgOverlay)
			} else {
				img = imgBg
			}
		} else if imgOverlay != nil {
			img = imagesCombine.CombineTwo(imagesBackgrounds.Black, imgOverlay)
		} else {
			img = imagesBackgrounds.Black
		}
		imgStr, errImg := streamdeck.Image(img)
		if errImg == nil {
			Cache[ctxStr].Images[i] = imgStr
		}
	}
}
