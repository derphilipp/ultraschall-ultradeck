package actionRouting

import (
	"context"
	"encoding/json"
	"image"
	"time"
	// 	"os"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"gitlab.com/derphilipp/ultraschall-ultradeck/images/backgrounds"
	"gitlab.com/derphilipp/ultraschall-ultradeck/images/full"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/client"
	"gitlab.com/derphilipp/ultraschall-ultradeck/webapi"
)

type MatrixSettings struct {
	Action      string `json:"action"`
	ActiveImage string
}

var Settings map[string]*MatrixSettings
var CurrentImage map[string]string

func init() {
	Settings = make(map[string]*MatrixSettings)
	CurrentImage = make(map[string]string)
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.routing")

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &MatrixSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &MatrixSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}
		client.SetTitle(ctx, s.Action, streamdeck.HardwareAndSoftware)

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &MatrixSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			Settings[event.Context] = s
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		CurrentImage[event.Context] = ""
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if ok {
			switch s.Action {
			case "magicrouting":
				oscClient.ToggleMagicRouting()
			case "preshow":
				oscClient.SetMatrixPreshow()
			case "recording":
				oscClient.SetMatrixRecording()
			case "editing":
				oscClient.SetMatrixEditing()
			case "onair":
				oscClient.ToggleOnAir()
			}
		}

		return nil
	})

	go func() {
		for range time.Tick(time.Second / 10) {
			for ctxStr := range Settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				//           s, _ := Settings[ctxStr]

				img := getTypeImage(ctxStr)
				if img != nil {
					bg, err1 := streamdeck.Image(img)
					if err1 == nil {
						client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
					}
				}
			}
		}
	}()
}

func getTypeImage(ctxStr string) image.Image {
	s := Settings[ctxStr]
	cI := CurrentImage[ctxStr]

	switch s.Action {
	case "magicrouting":
		if webApi.State.MagicRouting {
			if cI != "magicrouting_active" {
				CurrentImage[ctxStr] = "magicrouting_active"
				return imagesFull.MagicRoutingActive
			}
		} else {
			if cI != "magicrouting" {
				CurrentImage[ctxStr] = "magicrouting"
				return imagesFull.MagicRouting
			}
		}
	case "preshow":
		if webApi.State.RoutingMatrixPreshow {
			if cI != "preshow_active" {
				CurrentImage[ctxStr] = "preshow_active"
				return imagesFull.MatrixPreshowActive
			}
		} else {
			if cI != "preshow" {
				CurrentImage[ctxStr] = "preshow"
				return imagesFull.MatrixPreshow
			}
		}
	case "recording":
		if webApi.State.RoutingMatrixRecording {
			if cI != "recording_active" {
				CurrentImage[ctxStr] = "recording_active"
				return imagesFull.MatrixRecordingActive
			}
		} else {
			if cI != "recording" {
				CurrentImage[ctxStr] = "recording"
				return imagesFull.MatrixRecording
			}
		}
	case "editing":
		if webApi.State.RoutingMatrixEditing {
			if cI != "editing_active" {
				CurrentImage[ctxStr] = "editing_active"
				return imagesFull.MatrixEditingActive
			}
		} else {
			if cI != "editing" {
				CurrentImage[ctxStr] = "editing"
				return imagesFull.MatrixEditing
			}
		}
	case "onair":
		if webApi.State.OnAir {
			if cI != "onair_active" {
				CurrentImage[ctxStr] = "onair_active"
				return imagesFull.OnAirActive
			}
		} else {
			if cI != "onair" {
				CurrentImage[ctxStr] = "onair"
				return imagesFull.OnAir
			}
		}
	default:
		if cI != "black" {
			CurrentImage[ctxStr] = "black"
			return imagesBackgrounds.Black
		}
	}

	return nil
}
