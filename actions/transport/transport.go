package actionTransport

import (
	"context"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	_ "image/png"
	"os"
	"time"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"gitlab.com/derphilipp/ultraschall-ultradeck/images/full"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/client"
	"gitlab.com/derphilipp/ultraschall-ultradeck/osc/listener"
	"gitlab.com/derphilipp/ultraschall-ultradeck/webapi"
	// 	"../../images/backgrounds"
)

type TransportSettings struct {
	//   Track int `json:"track,string" min:"1" max:"99"`
	//   OnKeyPress string `json:"onkeypress"`
	//   OnKeyRelease string `json:"onkeyrelease"`
	Action string `json:"action"`
}

var Settings map[string]*TransportSettings
var CurrentImage map[string]string

func init() {
	Settings = make(map[string]*TransportSettings)
	CurrentImage = make(map[string]string)
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.transport")

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &TransportSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &TransportSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			Settings[event.Context] = s
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &TransportSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}
		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}

		// 		return client.SetTitle(ctx, oscListener.ReaperState.TimeString, streamdeck.HardwareAndSoftware)
		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		CurrentImage[event.Context] = ""
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if !ok {
			return fmt.Errorf("couldn't find settings for context %v", event.Context)
		}

		switch s.Action {
		case "us_gotostart":
			oscClient.TransportStart()
		case "stop":
			oscClient.Stop()
		case "play", "play_time":
			oscClient.Play()
		case "pause":
			oscClient.Pause()
		case "us_gotoend":
			oscClient.TransportEnd()
		case "repeat":
			oscClient.Repeat()
		case "record", "record_time":
			oscClient.Record()
		case "us_recordatend", "us_recordatend_time":
			oscClient.StartRecordingAtEnd()
		}

		return nil
	})
	//
	// 	action.RegisterHandler(streamdeck.KeyUp, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	// 		s, ok := Settings[event.Context]
	// 		if !ok {
	// 			return fmt.Errorf("couldn't find settings for context %v", event.Context)
	// 		}
	//
	//     switch s.OnKeyRelease {
	//       case "mute":
	//         oscClient.TrackMute(s.Track)
	//       case "unmute":
	//         oscClient.TrackUnmute(s.Track)
	//     }
	//
	//     return nil
	// 	})

	go func() {
		for range time.Tick(time.Second / 10) {
			for ctxStr := range Settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				s, _ := Settings[ctxStr]

				img := getTypeImage(ctxStr)
				if img != nil {
					Settings[ctxStr] = s
					bg, err1 := streamdeck.Image(img)
					if err1 == nil {
						client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
					}
				}

				if s.Action == "play_time" || s.Action == "record_time" || s.Action == "us_recordatend_time" {
					client.SetTitle(ctx, oscListener.ReaperState.TimeString, streamdeck.HardwareAndSoftware)
				}

			}
		}
	}()
}

func getTypeImage(ctxStr string) image.Image {
	s := Settings[ctxStr]
	cI := CurrentImage[ctxStr]

	switch s.Action {
	case "us_gotostart":
		if cI != "us_gotostart" {
			CurrentImage[ctxStr] = "us_gotostart"
			return imagesFull.TransportBegin
		}
	case "us_gotoend":
		if cI != "us_gotoend" {
			CurrentImage[ctxStr] = "us_gotoend"
			return imagesFull.TransportEnd
		}
	case "pause":
		if oscListener.ReaperState.Pause || webApi.State.RecordPause {
			if cI != "pause_active" {
				CurrentImage[ctxStr] = "pause_active"
				return imagesFull.TransportPauseActive
			}
		} else {
			if cI != "pause" {
				CurrentImage[ctxStr] = "pause"
				return imagesFull.TransportPause
			}
		}
	case "play", "play_time":
		if oscListener.ReaperState.Play || webApi.State.RecordPause {
			if cI != "play_active" {
				CurrentImage[ctxStr] = "play_active"
				return imagesFull.TransportPlayActive
			}
		} else {
			if cI != "play" {
				CurrentImage[ctxStr] = "play"
				return imagesFull.TransportPlay
			}
		}
	case "record", "record_time", "us_recordatend", "us_recordatend_time":
		if oscListener.ReaperState.Record || webApi.State.RecordPause {
			if cI != "record_active" {
				CurrentImage[ctxStr] = "record_active"
				return imagesFull.TransportRecordActive
			}
		} else {
			if cI != "record" {
				CurrentImage[ctxStr] = "record"
				return imagesFull.TransportRecord
			}
		}
	case "repeat":
		if oscListener.ReaperState.IfRepeat {
			if cI != "repeat_active" {
				CurrentImage[ctxStr] = "repeat_active"
				return imagesFull.TransportRepeatActive
			}
		} else {
			if cI != "repeat" {
				CurrentImage[ctxStr] = "repeat"
				return imagesFull.TransportRepeat
			}
		}
	case "stop":
		if cI != "stop" {
			CurrentImage[ctxStr] = "stop"
			return imagesFull.TransportStop
		}
	}

	return nil
}

func emptyImg() image.Image {
	img := image.NewRGBA(image.Rect(0, 0, 72, 72))
	for x := 0; x < 72; x++ {
		for y := 0; y < 72; y++ {
			img.Set(x, y, color.Black)
		}
	}
	return img
}

func loadImg(filename string) image.Image {
	infile, err := os.Open(filename)
	if err != nil {
		// replace this with real error handling
		panic(err.Error())
	}
	defer infile.Close()

	img, _, err := image.Decode(infile)
	if err != nil {
		// replace this with real error handling
		panic(err.Error())
	}

	return img
}
