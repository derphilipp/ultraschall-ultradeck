.PHONY: build clean test lint dep-update

VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
            echo v0)


build: build_macos_amd64 build_windows_amd64
	cd package && zip -r ../ultraschall.ultradeck.streamDeckPlugin fm.ultraschall.ultradeck.sdPlugin

build_macos_arm:
	echo "THIS IS NOT SUPPORTED YET"
	GOOS=darwin GOARCH=arm go build -v -o package/fm.ultraschall.ultradeck.sdPlugin/ultradeck
	cd package && zip -r ../fm.ultraschall.ultradeck.streamDeckPlugin fm.ultraschall.ultradeck.sdPlugin

build_macos_amd64:
	GOOS=darwin GOARCH=amd64 go build -v -o package/fm.ultraschall.ultradeck.sdPlugin/ultradeck

build_windows_amd64:
	GOOS=windows GOARCH=amd64 go build -v -o package/fm.ultraschall.ultradeck.sdPlugin/ultradeck.exe

test:
	go test -race -coverprofile=coverage.txt -covermode=atomic ./...

lint:
	golangci-lint run --enable-all --timeout 5m

dep-update:
	go get -u ./...
	go test ./...
	go mod tidy
	go mod vendor
